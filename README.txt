1) Create Workflows inside Workflows folder
2) To create new workflow copy "Template" from Workflowtemplate Folder. 
3) Every workflow which is linked with test case should contain Initialize and End Workflow Sequence as defined in Template Workflow.
4) Following Activities are present in Utils/Activities folder : 
    
   A)GetTestDataValue 
   @Input : Field,TestDataTable
   @Output : FieldValue (FieldValue contains data value)

   B)GetVP_Value
   @Input : Field,VPsDataTable
   @Output : FieldValue (FieldValue contains VP expected value)
   @Note : This activity is not used in most scenarios, Only use if you need VP expected value

   C)SetVP_Value
   @Input : Field,VPsDataTable,ActualValue
   @Output : (No Output)
   @Note : Use this activity to set actual values for VPs defined in RTA

   D)SetDynamicVP_Value
   @Input : Field,VPsDataTable,ActualValue,ExpectedValue
   @Output : (No Output)
   @Note : Use this activity to set dunamic VPs in RTA


5)Please don't modify any xaml defined in Utils folder